import React from 'react';
import { connect } from 'dva';
import router from 'umi/router';

import { getPSITokenId } from '../../../utils/tokenId';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import LeftArrowIcon from '@material-ui/icons/KeyboardArrowLeft'

import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import mainTheme from '../../../theme/mainTheme';
import LinearProgress from '@material-ui/core/LinearProgress';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import styles from '../../../utils/pageLayout';
import CustomerEditDialog from './CustomerEditDialog';
import MsgConfirm from '../../../utils/msgconfirm';
import MsgBox from '../../../utils/msgbox';
import * as customerService from '../services/customerService';

class CustomerDetailPage extends React.Component {
  state = {
    anchorEl: null,
    selectedTab: 0,
    editDialogOpen: false,
    msgConfirmOpen: false,
    msgConfirmInfo: '',
    msgBoxOpen: false,
    msgBoxInfo: '',
    deleteOK: false,
  }

  constructor(props) {
    super(props);

    if (!getPSITokenId()) {
      router.replace('/auth/login');
    }
  }

  tabChange = (event, value) => {
    this.setState({ selectedTab: value });
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  onQueryMenuItemClick = () => {
    this.setState({ anchorEl: null, queryDialogOpen: true });
  }

  render() {
    const { classes, loading, entity: customer } = this.props;
    const { anchorEl, selectedTab, editDialogOpen, msgConfirmOpen, msgConfirmInfo,
      msgBoxInfo, msgBoxOpen } = this.state;

    return (
      <MuiThemeProvider theme={mainTheme}>
        <CssBaseline />
        <Grid container alignItems='flex-start' wrap='nowrap' direction='column' className={classes.r}>
          <Grid item className={classes.h}>
            <AppBar position='static'>
              <Toolbar>
                <IconButton className={classes.menuButton} onClick={() => { router.goBack() }}>
                  <LeftArrowIcon color='inherit' />
                </IconButton>
                <Typography style={{ flex: 1 }} variant="title" color="inherit">
                  客户资料详情
                </Typography>
                <IconButton onClick={this.handleMenu}>
                  <MenuIcon color='inherit' />
                </IconButton>
                <Menu
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={anchorEl !== null}
                  onClose={() => { this.setState({ anchorEl: null }) }}>
                  <MenuItem onClick={this.onEditCustomer}>编辑当前客户资料</MenuItem>
                  <MenuItem onClick={this.onDeleteCustomer}>删除当前客户资料</MenuItem>
                </Menu>
              </Toolbar>
            </AppBar>
            <LinearProgress color='secondary' hidden={!loading} />
          </Grid>

          <Grid item className={classes.c}>
            <Tabs
              value={selectedTab}
              indicatorColor="primary"
              textColor="primary"
              onChange={this.tabChange}
            >
              <Tab label='主要' value={0}></Tab>
              <Tab label='联系人' value={1}></Tab>
              <Tab label='财务' value={2}></Tab>
              <Tab label='其他' value={3}></Tab>
            </Tabs>
            {
              selectedTab === 0 && customer &&
              <Table>
                <TableBody>
                  <TableRow><TableCell >分类</TableCell><TableCell>{customer.categoryName}</TableCell></TableRow>
                  <TableRow><TableCell >编码</TableCell><TableCell>{customer.code}</TableCell></TableRow>
                  <TableRow><TableCell >名称</TableCell><TableCell>{customer.name}</TableCell></TableRow>
                  <TableRow><TableCell >地址</TableCell><TableCell>{customer.address}</TableCell></TableRow>
                  <TableRow><TableCell >收货地址</TableCell><TableCell>{customer.addressReceipt}</TableCell></TableRow>
                </TableBody>
              </Table>
            }
            {
              selectedTab === 1 && customer &&
              <Table>
                <TableBody>
                  <TableRow><TableCell>联系人</TableCell><TableCell>{customer.contact01}</TableCell></TableRow>
                  <TableRow><TableCell>手机</TableCell><TableCell>{customer.mobile01}</TableCell></TableRow>
                  <TableRow><TableCell>固话</TableCell><TableCell>{customer.tel01}</TableCell></TableRow>
                  <TableRow><TableCell>QQ</TableCell><TableCell>{customer.qq01}</TableCell></TableRow>
                  <TableRow><TableCell>备用联系人</TableCell><TableCell>{customer.contact02}</TableCell></TableRow>
                  <TableRow><TableCell>备用手机</TableCell><TableCell>{customer.mobile02}</TableCell></TableRow>
                  <TableRow><TableCell>备用固话</TableCell><TableCell>{customer.tel02}</TableCell></TableRow>
                  <TableRow><TableCell>备用QQ</TableCell><TableCell>{customer.qq02}</TableCell></TableRow>
                </TableBody>
              </Table>
            }
            {
              selectedTab === 2 && customer &&
              <Table>
                <TableBody>
                  <TableRow><TableCell>开户行</TableCell><TableCell>{customer.banckName}</TableCell></TableRow>
                  <TableRow><TableCell>开户行账号</TableCell><TableCell>{customer.bankAccount}</TableCell></TableRow>
                  <TableRow><TableCell>税号</TableCell><TableCell>{customer.tax}</TableCell></TableRow>
                  <TableRow><TableCell>传真</TableCell><TableCell>{customer.fax}</TableCell></TableRow>
                  <TableRow><TableCell>期初余额</TableCell><TableCell>{customer.initReceivables}</TableCell></TableRow>
                  <TableRow><TableCell>期初余额日期</TableCell><TableCell>{customer.initReceivablesDT}</TableCell></TableRow>
                </TableBody>
              </Table>
            }
            {
              selectedTab === 3 && customer &&
              <Table>
                <TableBody>
                  <TableRow><TableCell>销售出库仓库</TableCell><TableCell>{customer.warehouseName}</TableCell></TableRow>
                  <TableRow><TableCell>备注</TableCell><TableCell>{customer.note}</TableCell></TableRow>
                </TableBody>
              </Table>
            }

            <CustomerEditDialog open={editDialogOpen}
              add={false}
              onOK={() => { this.setState({ editDialogOpen: false }) }}
              onCancel={() => { this.setState({ editDialogOpen: false }) }} />
            <MsgConfirm open={msgConfirmOpen} info={msgConfirmInfo}
              onOK={this.doDeleteCustomer}
              onCancel={() => { this.setState({ msgConfirmOpen: false }) }} />
            <MsgBox open={msgBoxOpen} info={msgBoxInfo} onOK={this.onDeleteOK} />
          </Grid>
        </Grid>
      </MuiThemeProvider>
    );
  }

  /**
   * 编辑客户资料
   */
  onEditCustomer = () => {
    this.setState({ anchorEl: null, editDialogOpen: true });
  }

  /**
   * 删除客户资料
   */
  onDeleteCustomer = () => {
    this.setState({ anchorEl: null, msgConfirmOpen: true, msgConfirmInfo: '请确认是否删除当前客户资料?' });
  }

  doDeleteCustomer = () => {
    this.setState({ msgConfirmOpen: false });

    const { entity, dispatch, page, qc } = this.props;

    customerService.deleteCustomer({ id: entity.id }).then(
      ({ data: result }) => {
        if (result.success) {
          dispatch({ type: 'customerList/queryCustomerList', payload: { page, qc } });
          this.setState({
            deleteOK: true,
            msgBoxInfo: '成功删除客户分类',
            msgBoxOpen: true
          });
        } else {
          // 显示错误信息
          this.setState({ deleteOK: false, msgBoxOpen: true, msgBoxInfo: result.msg });
        }
      }
    );
  }

  onDeleteOK = () => {
    this.setState({ msgBoxInfo: '', msgBoxOpen: false });

    const { deleteOK } = this.state;
    if (deleteOK) {
      // 删除成功
      router.replace('/customer');
    }
  }
}

CustomerDetailPage.propTypes = {
};

function mapStateToProps(state) {
  const { entity, page, qc } = state.customerList;

  return {
    entity, loading: state.loading.models.customerList,
    page, qc
  };
}

export default connect(mapStateToProps)(withStyles(styles)(CustomerDetailPage));
